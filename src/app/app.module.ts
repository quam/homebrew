import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { MainComponent } from './common/main/main.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { MonstersMenuComponent } from './nav/monsters-menu/monsters-menu.component';
import { MonstersComponent } from './monsters/monsters.component';
import { MenuComponent } from './common/menu/menu.component';
import { BeholderKinComponent } from './monsters/beholder-kin/beholder-kin.component';
import { MonsterComponent } from './common/monster/monster.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    MainMenuComponent,
    MonstersMenuComponent,
    MonstersComponent,
    MenuComponent,
    BeholderKinComponent,
    MonsterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
