import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-monsters-menu',
  templateUrl: './monsters-menu.component.html',
  styleUrls: ['./monsters-menu.component.scss']
})
export class MonstersMenuComponent implements OnInit {

  monsters: any = [
    {
      title: 'Beholder-kin',
      text: 'Beholders and their kin for an eye themed campaign.',
      link: '/monsters/beholder-kin',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
