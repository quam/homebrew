import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonstersMenuComponent } from './monsters-menu.component';

describe('MonstersMenuComponent', () => {
  let component: MonstersMenuComponent;
  let fixture: ComponentFixture<MonstersMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonstersMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonstersMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
