import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-beholder-kin",
  templateUrl: "./beholder-kin.component.html",
  styleUrls: ["./beholder-kin.component.scss"],
})
export class BeholderKinComponent implements OnInit {
  // ===================================== MONSTERS ========================================== //

  astereater: any = {
    name: "Astereater",
    size: "Large",
    type: "Aberration",
    ac: "17 (natural armor)",
    hp: "95 (10d10 + 40)",
    speed: "0 ft., fly 20ft. (hover)",
    str: "21 (+5)",
    dex: "9 (-1)",
    con: "19 (+4)",
    int: "8 (-1)",
    wis: "13 (+1)",
    cha: "11 (+0)",
    saving: "Str +8, Con +9",
    skill: "Perception +4, Stealth +2",
    damage_resistance:
      "bludgeoning, piercing, and slashing from nonmagical attacks",
    damage_immunity: "poison",
    condition_immunity: "petrified, poisoned, prone",
    sense: "Darkvision 120 ft., Passive Perception 15",
    languages: "Astereater",
    challenge: "6 (2300 xp)",
    abilities: [
      {
        name: "False Appearance",
        text: `While the Astereater remains motionless, it is indistinguishable
        from a normal asteroid.`,
      },
    ],
    actions: [
      {
        name: "Multiattack",
        text: `The astereater can use its bite attack or make two rock attacks.`,
      },
      {
        name: "Bite",
        text: `Melee Weapon Attack: +8 to hit, reach 5 ft., one
        target. Hit: 11 (1d12 + 5) piercing damage. If the target
        is medium or smaller and attack hits by more than 5 astereater
        swallows its target whole.
        While swallowed, the creature is blinded and restrained, it has
        total cover against attacks and other effects outside the astereater,
        and it takes takes 39 (6d12) acid damage at the beginning of each of the
        astereater’s turns. If the astereater takes 20 damage or more on a single
        turn from a creature inside it, the astereater must succeed on a DC 20
        Constitution saving throw at the end of that turn or regurgitate all
        swallowed creatures, which fall prone in a space within 10 feet of the
        astereater. If the astereater dies, a swallowed creature is no longer
        restrained by it and can escape from the corpse by using 30 feet of
        movement, exiting prone.`,
      },
      {
        name: "Rock",
        text: `Ranged Weapon Attack: +8 to hit, range 30/120 ft., one target.
        Hit: 16 (2d10 + 5) bludgeoning damage.`,
      },
    ],
  };

  lensman: any = {
    name: "Lensman",
    size: "Medium",
    type: "Aberration",
    ac: "16 (natural armor)",
    hp: "16 (3d8 + 3)",
    speed: "30 ft.",
    str: "16 (+3)",
    dex: "14 (+2)",
    con: "13 (+1)",
    int: "4 (-3)",
    wis: "12 (+1)",
    cha: "7 (-2)",
    skill: "Athletics +5",
    sense: "Darkvision 60 ft., Passive Perception 11",
    languages: "Deep speech",
    challenge: "1 (200 xp)",
    abilities: [
      {
        name: "Central Eye",
        text: `Once per day, as an action, lensman can use its central
        eye to force a creature in 30 ft. range that lensman can see to
        make a DC 11 Wisdom saving throw or become frightened of the lensman
        for 1 minute. The creature can repeat the saving throw at the end of
          each of its turns, ending the effect on success.`,
        /*
        additional: `
        <ol>
          <li><b>Fear.</b> A creature in 30 ft. range that lensman can
          see must succeed a DC 11 Wisdom saving throw or become
          frightened of the lensman.</li>
          <li><b>Dispel Magic.</b> Magical item in 30 ft. range from
          lensman loses its magical ability for 1 minute. Artifacts are
          immune to this ability.</li>
          <li><b>Phantasmal Force.</b> Lensman casts phantasmal force
          on a target it can see. Spell save DC is 11.</li>
          <li><b>Heal.</b> A target within 30 ft. that a lensman can
          see regains 1d6+1 hit points.</li>
          <li><b>Tongues.</b> Lensman casts comprehend languages on
          itself.</li>
          <li><b>Protection.</b> Lensman casts protection from poison
          on itself.</li>
        <ol>
        `*/
      },
    ],
    actions: [
      {
        name: "Multiattack",
        text: `Lensman makes two attacks with its double-headed spear.`,
      },
      {
        name: "Double-headed Spear",
        text: `Melee Weapon Attack: +5 to hit, reach 5 ft. One target. Hit:
        7 (1d8 + 3) piercing damage.`,
      },
    ],
  };

  examiner: any = {
    name: "Examiner",
    size: "Medium",
    type: "Aberration",
    ac: "14 (natural armor)",
    hp: "55 (10d8 + 10)",
    speed: "0 ft., fly 20ft. (hover)",
    str: "8 (-1)",
    dex: "16 (+3)",
    con: "12 (+1)",
    int: "18 (+4)",
    wis: "11 (+0)",
    cha: "12 (+1)",
    saving: "Dex +7, Con +11, Wis +8",
    skill:
      "Perception +3, Arcana +6, History +6, Sleight of Hands +5",
    condition_immunity: "prone",
    sense: "Darkvision 120 ft., Passive Perception 15",
    languages: "Deep Speech, Undercommon, Common",
    challenge: "3 (700 xp)",
    abilities: [
      {
        name: "Regeneration",
        text: `The beholder regains 10 hit points at the
        start of its turn if it has at least 1 hit point.
        `,
      },
      {
        name: "Spell Reflection",
        text: `If the examiner makes a successful saving throw against
        a spell, or a spell misses it, the spectator can choose another
        creature within 30 feet of it that it can see. The spell affects
        the chosen creature instead of the examiner.`,
      },
    ],
    actions: [
      {
        name: "Multiattack",
        text: `Examiner can do two rapier attacks, one crossbow attack and use
        one of its eyes at random`,
      },
      {
        name: "rapier",
        text: `Melee Weapon Attack: +5 to hit, reach 5 ft., one
        target. Hit: 7 (1d8 + 3) piercing damage.`,
      },
      {
        name: "Light Crossbow",
        text: `Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target.
        Hit: 7 (1d8 + 3) piercing damage.`,
      },
      {
        name: "Eye Rays",
        text: `The Examiner shoots 1 of the following
        magical eye rays at random,
        choosing a target it can see within 120
        feet of it:`,
      },
      {
        name: "1. Enlarge Ray",
        text: `The target's size doubles in all dimensions, and its
        weight is multiplied by eight. This growth increases its size
        by one category—from Medium to Large, for example. If there isn't
        enough room for the target to double its size, the creature or object
        attains the maximum possible size in the space available. This effect
        lasts for one minute. The target also has advantage on Strength checks
        and Strength saving throws. The target's weapons also grow to match its
        new size. While these weapons are enlarged, the target's attacks with
        them deal 1d4 extra damage.
        `,
      },
      {
        name: "2. Reduction Ray",
        text: `The target must make a DC 16 Constitution saving throw.
        On a failure, the target's size is halved in all dimensions,
        and its weight is reduced to one-eighth of normal. This
        reduction decreases its size by one category—from Medium to
        Small, for example. This effect last for 1 minute. The target
        also has disadvantage on Strength checks and Strength saving
        throws. The target's weapons also shrink to match its new size.
        While these weapons are reduced, the target's attacks with them
        deal 1d4 less damage (this can't reduce the damage below 1).`,
      },
      {
        name: "3. Transmute Ray",
        text: `Examiner targets an object of Medium size or smaller or
        a section of an object no more than 5 feet in any dimension and
        form it into any shape that suits its purpose. So, for example,
        examiner could shape a large rock into a weapon, idol, or coffer,
        or make a small passage through a wall, as long as the wall is
        less than 5 feet thick. Examiner could also shape a door or its
        frame to seal the door shut. The object you create can have up
        to two hinges and a latch, but finer mechanical detail isn't possible.`,
      },
      {
        name: "4. Identification Ray",
        text: `Examiner targets one object in range. If it is a magic item
        or some other magic-imbued object, examiner learns its properties
        and how to use them, whether it requires attunement to use, and how
        many charges it has, if any. Examiner learns whether any spells are
        affecting the item and what they are. If the item was created by a
        spell, examiner learns which spell created it.
        If targets the creature instead, it learns what spells, if any, are
        currently affecting it.`,
      },
    ],
  };

  overseer: any = {
    name: "Overseer",
    size: "Large",
    type: "Aberration",
    ac: "20 (natural armor, fungus)",
    hp: "184 (16d10 + 96)",
    speed: "5 ft.",
    str: "22 (+6)",
    dex: "13 (+1)",
    con: "22 (+6)",
    int: "20 (+5)",
    wis: "18 (+4)",
    cha: "22 (+6)",
    saving: "Dex +6, Con +11, Wis +9",
    skill:
      "Perception +9, Arcana +10, History +10, Medicine +10, Investigation +10, survival +9",
    sense: "Darkvision 120 ft., Passive Perception 19",
    languages: "Deep Speech, Common",
    challenge: "15 (13 000 xp)",
    abilities: [
      {
        name: "Command Beholder",
        text: `An overseer can cast dominate monster at will
        (DC 19), requiring no material components, but only
        against beholders and beholderkin. If an overseer loses
        control of a dominated beholder (as is the case if a
        dominated beholder is in the antimagic cone of another
        beholder), the overseer immediately senses the loss of
        control, and knows the position and distance to the beholder
        at the time control was lost. If control does not return
        within a few rounds, the overseer seeks out the
        rogue beholder to investigate and possibly punish it. Single
        overseer can control up to 22 beholders at the same time`,
      },
      {
        name: "The eye of Creation",
        text: `The beholder overseer can spend 10 minutes using its
        eye of creation to create any mundane item no bigger than 5 ft cube.
        The object is made of light, gray substance and degrades to dust in
        a year.`
      },
      {
        name: "The Eye of Spell Turning",
        text: `Once per turn, when overseer becomes target of a spell 
        or is included in its area of effect it can try to turn it against
        the caster. Make a spell attack contest. If Overseer wins the
        the caster becomes the target of the speel or the center of its area of
        effect instead. If caster wins the spell resolves as normal.`
      },
      {
        name: "The eye of Counterspell",
        text: `Overseer can cast counterspell at 3rd level once per turn
        without using reaction.`
      },
      {
        name: "Legendary Resistance (3/Day)",
        text: `If the beholder overseer fails a saving
        throw, it can choose to succeed instead.`,
      },
    ],
    actions: [
      {
        name: "Multiattack",
        text: `The overseer can use its bite attack and make
        its eye ray attacks on each of its turns.`,
      },
      {
        name: "Bite",
        text: `Melee Weapon Attack: +11 to hit, reach 5 ft., one
        target. Hit: 9 (1d6 + 6) piercing damage. The target must
        succeed a DC 21 Strength or Dexterity saving throw (its 
        choice) or be grappled by the overseer. While grappled the
        target takes 9 (1d6 + 6) piercing damage at the beginning of
        each of the overseer's turns and can use its action to try to
        repeat saving throw and escape.`,
      },
      {
        name: "Eye Rays",
        text: `The overseer shoots five of the following
        magical eye rays at random (reroll duplicates),
        choosing one to five targets it can see within 120
        feet of it:`,
      },
      {
        name: "1. Charm Ray",
        text: `The targeted creature must succeed on a DC
        22 Wisdom saving throw or be charmed by the hive mother for 1
        hour, or until the hive mother harms the creature.
        `,
      },
      {
        name: "2. Paralyzing Ray",
        text: `The targeted creature must succeed on a DC 22
        Constitution saving throw or be paralyzed for 1 minute. The target can
        repeat the saving throw at the end of each of its turns, ending the
        effect on itself on a success.`,
      },
      {
        name: "3. Fear Ray",
        text: `The targeted creature must succeed on a DC 22
        Wisdom saving throw or be frightened for 1 minute. The target
        can repeat the saving throw at the end of each of its turns,
        ending the effect on itself on a success. `,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 22 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "5. Enervation Ray",
        text: `The targeted creature must make a DC 22 Constitution saving
        throw, taking 54 (12d8) necrotic damage on a failed save, or half as much damage on
        a successful one.`,
      },
      {
        name: "6. Telekinetic Ray",
        text: `If the target is a creature, it must succeed on a DC 22 Strength saving throw
        or the hive mother moves it up to 30 feet in any direction. It is restrained by the
        ray's telekinetic grip until the start of the hive mother's next turn or until the hive mother
        is incapacitated. 
        If the target is an object weighing 300 pounds or less that isn't being worn or carried,
        it is moved up to 30 feet in any direction. The hive mother can also exert fine control on
        objects with this ray, such as manipulating a simple tool or opening a door or a container.`,
      },
      {
        name: "7. Sleep Ray",
        text: `The targeted creature must succeed on a DC 22
        Wisdom saving throw or fall asleep and remain unconscious
        for 1 minute. The target awakens if it takes damage or another
        creature takes an action to wake it. This ray has no effect on
        constructs and undead.`,
      },
      {
        name: "8. Petrification Ray",
        text: `The targeted creature must make a DC 22 Dexterity saving throw. On a failed
        save, the creature begins to turn to stone and is restrained. It must repeat the
        saving throw at the end of its next turn. On a success, the effect ends. On a failure,
        the creature is petrified until freed by the greater restoration spell or other magic.`,
      },
      {
        name: "9. Disintegration Ray",
        text: `If the target is a creature, it must succeed on a DC 22 Dexterity saving throw
        or take 45 (10d8) force damage. If this damage reduces the creature to 0 hit points,
        its body becomes a pile of fine gray dust.
        If the target is a Large or smaller nonmagical object or creation of magical force,
        it is disintegrated without a saving throw. If the target is a Huge or larger object
        or creation of magical force, this ray disintegrates a 10-foot cube of it.`,
      },
      {
        name: "10. Death Ray",
        text: `The targeted creature must succeed on a DC 22 Dexterity saving throw or take
        71 (13d10) necrotic damage. The target dies if the ray reduces it to 0 hit points.`,
      },
    ],
    legendary_description: `Hive mother can take 3 legendary actions, using the Eye Ray option
    below. It can take only one legendary action at a time and only at the end of another
    creature's turn. Hive Mother regains spent legendary actions at the start of its turn.`,
    legendary_actions: [
      {
        name: "Eye Ray",
        text: `The hive mother uses one random eye ray.`,
      },
    ],
  };

  hive_mother: any = {
    name: "Hive Mother",
    size: "Huge",
    type: "Aberration",
    ac: "19 (natural armor)",
    hp: "263 (22d12 + 120)",
    speed: "5 ft., fly 20ft. (hover)",
    str: "24 (+7)",
    dex: "14 (+2)",
    con: "22 (+6)",
    int: "21 (+5)",
    wis: "17 (+3)",
    cha: "25 (+7)",
    saving: "Con +11, Wis +8, Cha +12",
    skill:
      "Perception +8, Stealth +7, Intimidation +12, Arcana +10, History +10, Investigation +10",
    condition_immunity: "prone",
    sense: "Darkvision 120 ft., Passive Perception 18",
    languages: "Deep Speech, Common, 2 others",
    challenge: "15 (13000 xp)",
    abilities: [
      {
        name: "Antimagic Cone",
        text: `the Hive mother's central eye creates an area
        of antimagic, as in the antimagic field spell, in
        a 150-foot cone. At the start of each of its turns,
        the beholder decides which way the cone faces and
        whether the cone is active. The area works against
        the beholder's own eye rays.`,
      },
      {
        name: "Command Beholder",
        text: `A hive mother can cast dominate monster at will
        (DC 22), requiring no material components, but only
        against beholders and beholderkin. Hive mothers are
        immune to the command beholder ability of other hive
        mothers. If a hive mother loses control of a dominated
        beholder (as is the case if a dominated beholder is in
        the antimagic cone of another beholder), the hive
        mother immediately senses the loss of control, and
        knows the position and distance to the beholder at
        the time control was lost. If control does not return
        within a few rounds, the hive mother seeks out the
        rogue beholder to investigate and possibly punish it.`,
      },
      {
        name: "Legendary Resistance (3/Day)",
        text: `If the hive mother fails a saving
        throw, it can choose to succeed instead.`,
      },
    ],
    actions: [
      {
        name: "Multiattack",
        text: `The hive mother can use its bite attack and make
        its eye ray attacks on each of its turns.`,
      },
      {
        name: "Bite",
        text: `Melee Weapon Attack: +12 to hit, reach 5 ft., one
        target. Hit: 11 (2d6 + 7) piercing damage.  If the target
        is a Medium or smaller creature, it must succeed on a DC 22
        Dexterity saving throw or be swallowed by the hive mother.
        While swallowed, the creature is blinded and restrained, it has
        total cover against attacks and other effects outside the hive mother,
        and it takes 16 (2d8+7) bludgeoning and 9 (2d8) acid damage at the
        beginning of each of the hive mother’s turns. If the hive mother takes
        15 damage or more on a single turn from a creature inside it, the
        hive mother must succeed on a DC 20 Constitution saving throw at the end
        of that turn or regurgitate all swallowed creatures, which fall prone in
        a space within 10 feet of the hive mother. If the hive mother dies, a
        swallowed creature is no longer restrained by it and can escape from the
        corpse by using 30 feet of movement, exiting prone.`,
      },
      {
        name: "Eye Rays",
        text: `The hive mother shoots six of the following
        magical eye rays at random (reroll duplicates),
        choosing one to six targets it can see within 120
        feet of it:`,
      },
      {
        name: "1. Charm Ray",
        text: `The targeted creature must succeed on a DC
        22 Wisdom saving throw or be charmed by the hive mother for 1
        hour, or until the hive mother harms the creature.
        `,
      },
      {
        name: "2. Paralyzing Ray",
        text: `The targeted creature must succeed on a DC 22
        Constitution saving throw or be paralyzed for 1 minute. The target can
        repeat the saving throw at the end of each of its turns, ending the
        effect on itself on a success.`,
      },
      {
        name: "3. Fear Ray",
        text: `The targeted creature must succeed on a DC 22
        Wisdom saving throw or be frightened for 1 minute. The target
        can repeat the saving throw at the end of each of its turns,
        ending the effect on itself on a success. `,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 22 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "5. Enervation Ray",
        text: `The targeted creature must make a DC 22 Constitution saving
        throw, taking 54 (12d8) necrotic damage on a failed save, or half as much damage on
        a successful one.`,
      },
      {
        name: "6. Telekinetic Ray",
        text: `If the target is a creature, it must succeed on a DC 22 Strength saving throw
        or the hive mother moves it up to 30 feet in any direction. It is restrained by the
        ray's telekinetic grip until the start of the hive mother's next turn or until the hive mother
        is incapacitated. 
        If the target is an object weighing 300 pounds or less that isn't being worn or carried,
        it is moved up to 30 feet in any direction. The hive mother can also exert fine control on
        objects with this ray, such as manipulating a simple tool or opening a door or a container.`,
      },
      {
        name: "7. Sleep Ray",
        text: `The targeted creature must succeed on a DC 22
        Wisdom saving throw or fall asleep and remain unconscious
        for 1 minute. The target awakens if it takes damage or another
        creature takes an action to wake it. This ray has no effect on
        constructs and undead.`,
      },
      {
        name: "8. Petrification Ray",
        text: `The targeted creature must make a DC 22 Dexterity saving throw. On a failed
        save, the creature begins to turn to stone and is restrained. It must repeat the
        saving throw at the end of its next turn. On a success, the effect ends. On a failure,
        the creature is petrified until freed by the greater restoration spell or other magic.`,
      },
      {
        name: "9. Disintegration Ray",
        text: `If the target is a creature, it must succeed on a DC 22 Dexterity saving throw
        or take 45 (10d8) force damage. If this damage reduces the creature to 0 hit points,
        its body becomes a pile of fine gray dust.
        If the target is a Large or smaller nonmagical object or creation of magical force,
        it is disintegrated without a saving throw. If the target is a Huge or larger object
        or creation of magical force, this ray disintegrates a 10-foot cube of it.`,
      },
      {
        name: "10. Death Ray",
        text: `The targeted creature must succeed on a DC 22 Dexterity saving throw or take
        71 (13d10) necrotic damage. The target dies if the ray reduces it to 0 hit points.`,
      },
    ],
    legendary_description: `Hive mother can take 3 legendary actions, using the Eye Ray option
    below. It can take only one legendary action at a time and only at the end of another
    creature's turn. Hive Mother regains spent legendary actions at the start of its turn.`,
    legendary_actions: [
      {
        name: "Eye Ray",
        text: `The hive mother uses one random eye ray.`,
      },
    ],
  };

  // ===================================== NAMED ========================================== //

  amnaxir: any = {
    name: "Amnaxir, The Greatest",
    size: "Huge",
    type: "Aberration",
    ac: "18 (natural armor)",
    hp: "199 (19d12 + 76)",
    speed: "0 ft., fly 20ft. (hover)",
    str: "14 (+2)",
    dex: "14 (+2)",
    con: "18 (+4)",
    int: "17 (+3)",
    wis: "15 (+2)",
    cha: "17 (+3)",
    saving: "Str +7 Int +8, Cha +8",
    skill: "Perception +12",
    condition_immunity: "prone",
    sense: "darkvision 120 ft., passive Perception 22",
    languages: "Deep Speech, Undercommon, Giant, Common",
    challenge: "14 (11500 xp)",
    abilities: [
      {
        name: "Antimagic Cone",
        text: `The beholder's central eye creates an area
        of antimagic, as in the antimagic field spell, in
        a 150-foot cone. At the start of each of its turns,
        the beholder decides which way the cone faces and
        whether the cone is active. The area works against
        the beholder's own eye rays.`,
      },
      {
        name: "Enlarged",
        text: `Amnaxir has advantage on all strength checks
        and strength saving throws.`,
      },
    ],
    actions: [
      {
        name: "Eye Rays",
        text: `The beholder shoots three of the following
        magical eye rays at random (reroll duplicates),
        choosing one to three targets it can see within 120
        feet of it:`,
      },
      {
        name: "1. Reduction Ray",
        text: `The target must make a DC 16 Constitution saving throw.
        On a failure, the target's size is halved in all dimensions,
        and its weight is reduced to one-eighth of normal. This
        reduction decreases its size by one category—from Medium to
        Small, for example. This effect last for 1 minute. The target
        also has disadvantage on Strength checks and Strength saving
        throws. The target's weapons also shrink to match its new size.
        While these weapons are reduced, the target's attacks with them
        deal 1d4 less damage (this can't reduce the damage below 1).`,
      },
      {
        name: "2. Paralyzing Ray",
        text: `The targeted creature must succeed on a DC 16
        Constitution saving throw or be paralyzed for 1 minute. The target can
        repeat the saving throw at the end of each of its turns, ending the
        effect on itself on a success.`,
      },
      {
        name: "3. Fear Ray",
        text: `The targeted creature must succeed on a DC 16
        Wisdom saving throw or be frightened for 1 minute. The target
        can repeat the saving throw at the end of each of its turns,
        ending the effect on itself on a success. `,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "5. Enervation Ray",
        text: `The targeted creature must make a DC 16 Constitution saving
        throw, taking 36 (8d8) necrotic damage on a failed save, or half as much damage on
        a successful one.`,
      },
      {
        name: "6. Telekinetic Ray",
        text: `If the target is a creature, it must succeed on a DC 16 Strength saving throw
        or the beholder moves it up to 30 feet in any direction. It is restrained by the
        ray's telekinetic grip until the start of the beholder's next turn or until the beholder
        is incapacitated. 
        If the target is an object weighing 300 pounds or less that isn't being worn or carried,
        it is moved up to 30 feet in any direction. The beholder can also exert fine control on
        objects with this ray, such as manipulating a simple tool or opening a door or a container.`,
      },
      {
        name: "7. Silencing Ray",
        text: `The targeted creature must succeed a DC 16 Wisdom saving throw
        or be silenced as if under effect of silence spell. The area of silence created
        with this eye is 5 feet square and moves with a creature staying centered on it.
        At the end of each of its turn the creature can repeat wisdom saving throw, ending
        the effect on success.`,
      },
      {
        name: "8. Petrification Ray",
        text: `The targeted creature must make a DC 16 Dexterity saving throw. On a failed
        save, the creature begins to turn to stone and is restrained. It must repeat the
        saving throw at the end of its next turn. On a success, the effect ends. On a failure,
        the creature is petrified until freed by the greater restoration spell or other magic.`,
      },
      {
        name: "9. Disintegration Ray",
        text: `If the target is a creature, it must succeed on a DC 16 Dexterity saving throw
        or take 45 (10d8) force damage. If this damage reduces the creature to 0 hit points,
        its body becomes a pile of fine gray dust.
        If the target is a Large or smaller nonmagical object or creation of magical force,
        it is disintegrated without a saving throw. If the target is a Huge or larger object
        or creation of magical force, this ray disintegrates a 10-foot cube of it.`,
      },
      {
        name: "10. Death Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity saving throw or take
        55 (10d10) necrotic damage. The target dies if the ray reduces it to 0 hit points.`,
      },
    ],
    legendary_description: `Amnaxir can take 3 legendary actions, using the Eye Ray option
    below. It can take only one legendary action at a time and only at the end of another
    creature's turn. Amnaxir regains spent legendary actions at the start of its turn.`,
    legendary_actions: [
      {
        name: "Eye Ray",
        text: `Amnaxir uses one random eye ray.`,
      },
    ],
  };

  orox: any = {
    name: "Orox, Bane of Elves",
    size: "Large",
    type: "Aberration",
    ac: "20 (natural armor + shield), 22 at range (Arrow-Catching Shield)",
    hp: "180 (19d10 + 76)",
    speed: "0 ft., fly 20ft. (hover)",
    str: "10 (+0)",
    dex: "14 (+2)",
    con: "18 (+4)",
    int: "17 (+3)",
    wis: "15 (+2)",
    cha: "17 (+3)",
    saving: "Int +8, Wis +7, Cha +8",
    skill: "Perception +12",
    condition_immunity: "prone",
    sense: "darkvision 120 ft., passive Perception 22",
    languages: "Deep Speech, Undercommon, Common, Elven",
    challenge: "14 (11500 xp)",
    abilities: [
      {
        name: "Antimagic Cone",
        text: `The beholder's central eye creates an area
        of antimagic, as in the antimagic field spell, in
        a 150-foot cone. At the start of each of its turns,
        the beholder decides which way the cone faces and
        whether the cone is active. The area works against
        the beholder's own eye rays.`,
      },
      {
        name: "Arrow-Catching Shield",
        text: `Orox possesses an arrow catching shield that
        he fused to his body, granting him +2 AC and additional
        +2 AC against ranged attack. Orox, however never uses
        shield's ability of switching target and it is unknown
        whether that ability still works in this form.`,
      },
    ],
    actions: [
      {
        name: "Eye Rays",
        text: `The beholder shoots three of the following
        magical eye rays at random (reroll duplicates),
        choosing one to three targets it can see within 120
        feet of it:`,
      },
      {
        name: "1. Acid Blast",
        text: `Target and all other creatures within 5 feet of
        it must make a DC 16 Dexterity saving throw, taking
        33 (6d10) acid damage on a failed save, or half as much
        on success.`,
      },
      {
        name: "2. Paralyzing Ray",
        text: `The targeted creature must succeed on a DC 16
        Constitution saving throw or be paralyzed for 1 minute. The target can
        repeat the saving throw at the end of each of its turns, ending the
        effect on itself on a success.`,
      },
      {
        name: "3. Exhaustion",
        text: `The targeted creature must succeed on a DC 16
        Constitution saving throw or immediately take one point of exhaustion.`,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "5. Enervation Ray",
        text: `The targeted creature must make a DC 16 Constitution saving
        throw, taking 36 (8d8) necrotic damage on a failed save, or half as much damage on
        a successful one.`,
      },
      {
        name: "6. Telekinetic Ray",
        text: `If the target is a creature, it must succeed on a DC 16 Strength saving throw
        or the beholder moves it up to 30 feet in any direction. It is restrained by the
        ray's telekinetic grip until the start of the beholder's next turn or until the beholder
        is incapacitated. 
        If the target is an object weighing 300 pounds or less that isn't being worn or carried,
        it is moved up to 30 feet in any direction. The beholder can also exert fine control on
        objects with this ray, such as manipulating a simple tool or opening a door or a container.`,
      },
      {
        name: "7. Silencing Ray",
        text: `The targeted creature must succeed a Wisdom saving throw
        or be silenced as if under effect of silence spell. The area of silence created
        with this eye is 5 feet square and moves with a creature staying centered on it.
        At the end of each of its turn the creature can repeat wisdom saving throw, ending
        the effect on success.`,
      },
      {
        name: "8. Petrification Ray",
        text: `The targeted creature must make a DC 16 Dexterity saving throw. On a failed
        save, the creature begins to turn to stone and is restrained. It must repeat the
        saving throw at the end of its next turn. On a success, the effect ends. On a failure,
        the creature is petrified until freed by the greater restoration spell or other magic.`,
      },
      {
        name: "9. Disintegration Ray",
        text: `If the target is a creature, it must succeed on a DC 16 Dexterity saving throw
        or take 45 (10d8) force damage. If this damage reduces the creature to 0 hit points,
        its body becomes a pile of fine gray dust.
        If the target is a Large or smaller nonmagical object or creation of magical force,
        it is disintegrated without a saving throw. If the target is a Huge or larger object
        or creation of magical force, this ray disintegrates a 10-foot cube of it.`,
      },
      {
        name: "10. Death Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity saving throw or take
        55 (10d10) necrotic damage. The target dies if the ray reduces it to 0 hit points.`,
      },
    ],
    legendary_description: `Orox can take 3 legendary actions, using the Eye Ray option
    below. It can take only one legendary action at a time and only at the end of another
    creature's turn. Orox regains spent legendary actions at the start of its turn.`,
    legendary_actions: [
      {
        name: "Eye Ray",
        text: `Orox uses one random eye ray.`,
      },
    ],
  };

  qualnus: any = {
    name: "Qualnus, the Spider Queen",
    size: "Huge",
    type: "Aberration (Hive Mother)",
    ac: "19 (natural armor)",
    hp: "263 (22d12 + 120)",
    speed: "40 ft., fly 20ft. (hover)",
    str: "24 (+7)",
    dex: "14 (+2)",
    con: "22 (+6)",
    int: "21 (+5)",
    wis: "17 (+3)",
    cha: "25 (+7)",
    saving: "Con +11, Wis +8, Cha +12",
    skill:
      "Perception +8, Stealth +7, Intimidation +12, Arcana +10, History +10, Investigation +10",
    condition_immunity: "prone",
    sense: "Darkvision 120 ft., Passive Perception 18",
    languages: "Deep Speech, Common, 2 others",
    challenge: "15 (13000 xp)",
    abilities: [
      {
        name: "Antimagic Cone",
        text: `the Hive mother's central eye creates an area
        of antimagic, as in the antimagic field spell, in
        a 150-foot cone. At the start of each of its turns,
        the beholder decides which way the cone faces and
        whether the cone is active. The area works against
        the beholder's own eye rays.`,
      },
      {
        name: "Command Beholder",
        text: `A hive mother can cast dominate monster at will
        (DC 22), requiring no material components, but only
        against beholders and beholderkin. Hive mothers are
        immune to the command beholder ability of other hive
        mothers. If a hive mother loses control of a dominated
        beholder (as is the case if a dominated beholder is in
        the antimagic cone of another beholder), the hive
        mother immediately senses the loss of control, and
        knows the position and distance to the beholder at
        the time control was lost. If control does not return
        within a few rounds, the hive mother seeks out the
        rogue beholder to investigate and possibly punish it.`,
      },
      {
        name: "Spider Legs",
        text: `Qualnus, the Spider Queen has 4 pairs of spider
        legs giving her movement speed of 40 ft`,
      },
      {
        name: "Legendary Resistance (3/Day)",
        text: `If the hive mother fails a saving
        throw, it can choose to succeed instead.`,
      },
    ],
    actions: [
      {
        name: "Multiattack",
        text: `The hive mother can use its bite attack and make
        its eye ray attacks on each of its turns.`,
      },
      {
        name: "Bite",
        text: `Melee Weapon Attack: +12 to hit, reach 5 ft., one
        target. Hit: 11 (2d6 + 7) piercing damage.  If the target
        is a Medium or smaller creature, it must succeed on a DC 22
        Dexterity saving throw or be swallowed by the hive mother.
        While swallowed, the creature is blinded and restrained, it has
        total cover against attacks and other effects outside the hive mother,
        and it takes 16 (2d8+7) bludgeoning and 9 (2d8) acid damage at the
        beginning of each of the hive mother’s turns. If the hive mother takes
        15 damage or more on a single turn from a creature inside it, the
        hive mother must succeed on a DC 20 Constitution saving throw at the end
        of that turn or regurgitate all swallowed creatures, which fall prone in
        a space within 10 feet of the hive mother. If the hive mother dies, a
        swallowed creature is no longer restrained by it and can escape from the
        corpse by using 30 feet of movement, exiting prone.`,
      },
      {
        name: "Eye Rays",
        text: `The hive mother shoots six of the following
        magical eye rays at random (reroll duplicates),
        choosing one to six targets it can see within 120
        feet of it:`,
      },
      {
        name: "1. Charm Ray",
        text: `The targeted creature must succeed on a DC
        22 Wisdom saving throw or be charmed by the hive mother for 1
        hour, or until the hive mother harms the creature.
        `,
      },
      {
        name: "2. Paralyzing Ray",
        text: `The targeted creature must succeed on a DC 22
        Constitution saving throw or be paralyzed for 1 minute. The target can
        repeat the saving throw at the end of each of its turns, ending the
        effect on itself on a success.`,
      },
      {
        name: "3. Fear Ray",
        text: `The targeted creature must succeed on a DC 22
        Wisdom saving throw or be frightened for 1 minute. The target
        can repeat the saving throw at the end of each of its turns,
        ending the effect on itself on a success. `,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 22 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "5. Enervation Ray",
        text: `The targeted creature must make a DC 22 Constitution saving
        throw, taking 54 (12d8) necrotic damage on a failed save, or half as much damage on
        a successful one.`,
      },
      {
        name: "6. Web Ray",
        text: `The targeted creature must succeed on a DC 22
        Dexterity saving throw or be restrained by webs. A
        creature restrained by the webs can use its action to
        make a DC 22 Strength check. If it succeeds, it is no
        longer restrained.
        The webs are flammable. If exposed to fire it burns away
        in 1 round, dealing 2d4 fire damage to a trapped creature.`,
      },
      {
        name: "7. Sleep Ray",
        text: `The targeted creature must succeed on a DC 22
        Wisdom saving throw or fall asleep and remain unconscious
        for 1 minute. The target awakens if it takes damage or another
        creature takes an action to wake it. This ray has no effect on
        constructs and undead.`,
      },
      {
        name: "8. Petrification Ray",
        text: `The targeted creature must make a DC 22 Dexterity saving throw. On a failed
        save, the creature begins to turn to stone and is restrained. It must repeat the
        saving throw at the end of its next turn. On a success, the effect ends. On a failure,
        the creature is petrified until freed by the greater restoration spell or other magic.`,
      },
      {
        name: "9. Disintegration Ray",
        text: `If the target is a creature, it must succeed on a DC 22 Dexterity saving throw
        or take 45 (10d8) force damage. If this damage reduces the creature to 0 hit points,
        its body becomes a pile of fine gray dust.
        If the target is a Large or smaller nonmagical object or creation of magical force,
        it is disintegrated without a saving throw. If the target is a Huge or larger object
        or creation of magical force, this ray disintegrates a 10-foot cube of it.`,
      },
      {
        name: "10. Death Ray",
        text: `The targeted creature must succeed on a DC 22 Dexterity saving throw or take
        71 (13d10) necrotic damage. The target dies if the ray reduces it to 0 hit points.`,
      },
    ],
    legendary_description: `Hive mother can take 3 legendary actions, using the Eye Ray option
    below. It can take only one legendary action at a time and only at the end of another
    creature's turn. Hive Mother regains spent legendary actions at the start of its turn.`,
    legendary_actions: [
      {
        name: "Eye Ray",
        text: `The hive mother uses one random eye ray.`,
      },
    ],
  };

  xeo: any = {
    name: "Xeo-Chem, Mage Killer Extraordinaire",
    size: "Large",
    type: "Aberration",
    ac: "20 (natural armor + shield), 22 at range (Arrow-Catching Shield)",
    hp: "180 (19d10 + 76)",
    speed: "0 ft., fly 20ft. (hover)",
    str: "10 (+0)",
    dex: "14 (+2)",
    con: "18 (+4)",
    int: "17 (+3)",
    wis: "15 (+2)",
    cha: "17 (+3)",
    saving: "Int +8, Wis +7, Cha +8",
    skill: "Perception +12",
    condition_immunity: "prone",
    sense: "darkvision 120 ft., passive Perception 22",
    languages: "Deep Speech, Undercommon, Elven",
    challenge: "14 (11500 xp)",
    abilities: [
      {
        name: "Antimagic Cones",
        text: `Xeo-Chem has 2 central eyes on opposite sides of the head.
        Both creates an area of antimagic, as in the antimagic field spell, in
        a 150-foot cone. At the start of each of its turns, Xeo-Chem decides
        which way the cones face (but they always face opposite directions) and
        whether the cones are active. The areas works against the beholder's own
        eye rays.`,
      },
    ],
    actions: [
      {
        name: "Eye Rays",
        text: `The beholder shoots three of the following
        magical eye rays at random (reroll duplicates),
        choosing one to three targets it can see within 120
        feet of it:`,
      },
      {
        name: "1. Acid Blast",
        text: `Target and all other creatures within 5 feet of
        it must make a DC 16 Dexterity saving throw, taking
        33 (6d10) acid damage on a failed save, or half as much
        on success.`,
      },
      {
        name: "2. Paralyzing Ray",
        text: `The targeted creature must succeed on a DC 16
        Constitution saving throw or be paralyzed for 1 minute. The target can
        repeat the saving throw at the end of each of its turns, ending the
        effect on itself on a success.`,
      },
      {
        name: "3. Exhaustion",
        text: `The targeted creature must succeed on a DC 16
        Constitution saving throw or immediately take one point of exhaustion.`,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "4. Slowing Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity
        saving throw. On a failed save, the target's speed is halved for 1 minute.
        In addition, the creature can't take reactions, and it can take either an action
        or a bonus action on its turn, not both. The creature can repeat the saving throw
        at the end of each of its turns, ending the effect on itself on a success.`,
      },
      {
        name: "5. Enervation Ray",
        text: `The targeted creature must make a DC 16 Constitution saving
        throw, taking 36 (8d8) necrotic damage on a failed save, or half as much damage on
        a successful one.`,
      },
      {
        name: "6. Telekinetic Ray",
        text: `If the target is a creature, it must succeed on a DC 16 Strength saving throw
        or the beholder moves it up to 30 feet in any direction. It is restrained by the
        ray's telekinetic grip until the start of the beholder's next turn or until the beholder
        is incapacitated. 
        If the target is an object weighing 300 pounds or less that isn't being worn or carried,
        it is moved up to 30 feet in any direction. The beholder can also exert fine control on
        objects with this ray, such as manipulating a simple tool or opening a door or a container.`,
      },
      {
        name: "7. Silencing Ray",
        text: `The targeted creature must succeed a Wisdom saving throw
        or be silenced as if under effect of silence spell. The area of silence created
        with this eye is 5 feet square and moves with a creature staying centered on it.
        At the end of each of its turn the creature can repeat wisdom saving throw, ending
        the effect on success.`,
      },
      {
        name: "8. Petrification Ray",
        text: `The targeted creature must make a DC 16 Dexterity saving throw. On a failed
        save, the creature begins to turn to stone and is restrained. It must repeat the
        saving throw at the end of its next turn. On a success, the effect ends. On a failure,
        the creature is petrified until freed by the greater restoration spell or other magic.`,
      },
      {
        name: "9. Disintegration Ray",
        text: `If the target is a creature, it must succeed on a DC 16 Dexterity saving throw
        or take 45 (10d8) force damage. If this damage reduces the creature to 0 hit points,
        its body becomes a pile of fine gray dust.
        If the target is a Large or smaller nonmagical object or creation of magical force,
        it is disintegrated without a saving throw. If the target is a Huge or larger object
        or creation of magical force, this ray disintegrates a 10-foot cube of it.`,
      },
      {
        name: "10. Death Ray",
        text: `The targeted creature must succeed on a DC 16 Dexterity saving throw or take
        55 (10d10) necrotic damage. The target dies if the ray reduces it to 0 hit points.`,
      },
    ],
    legendary_description: `Xeo-Chem can take 3 legendary actions, using the Eye Ray option
    below. It can take only one legendary action at a time and only at the end of another
    creature's turn. Xeo-Chem regains spent legendary actions at the start of its turn.`,
    legendary_actions: [
      {
        name: "Eye Ray",
        text: `Xeo-Chem uses one random eye ray.`,
      },
    ],
  };

  constructor() {}

  ngOnInit(): void {}
}
