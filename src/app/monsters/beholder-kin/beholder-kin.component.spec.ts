import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeholderKinComponent } from './beholder-kin.component';

describe('BeholderKinComponent', () => {
  let component: BeholderKinComponent;
  let fixture: ComponentFixture<BeholderKinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeholderKinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeholderKinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
