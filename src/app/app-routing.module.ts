import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { BeholderKinComponent } from './monsters/beholder-kin/beholder-kin.component';
import { MonstersComponent } from './monsters/monsters.component';

const extraOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
  onSameUrlNavigation: 'reload',
  useHash: true,
};

const routes: Routes = [
  {
    path: '',
    component: MainMenuComponent,
  },
  {
    path: 'monsters',
    component: MonstersComponent,
  },
  {
    path: 'monsters/beholder-kin',
    component: BeholderKinComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, extraOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
