import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-line-menu',
  templateUrl: './line-menu.component.html',
  styleUrls: ['./line-menu.component.scss']
})
export class LineMenuComponent implements OnInit {

  @Input()data: any;

  constructor() { }

  ngOnInit(): void {
  }

}
