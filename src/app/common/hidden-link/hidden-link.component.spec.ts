import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HiddenLinkComponent } from './hidden-link.component';

describe('HiddenLinkComponent', () => {
  let component: HiddenLinkComponent;
  let fixture: ComponentFixture<HiddenLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HiddenLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HiddenLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
