import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hidden-link',
  templateUrl: './hidden-link.component.html',
  styleUrls: ['./hidden-link.component.scss']
})
export class HiddenLinkComponent implements OnInit {

  @Input()link: String = "";
  @Input()text: String = "";

  constructor() {
  }

  ngOnInit(): void {
  }

  show_hidden() {
    return localStorage.getItem('ShowHidden') == 'true'
  }

}
